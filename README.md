# LODAtlas
LODAtlas is a web tool that helps users find linked data datasets of interest through keyword and URI search on datasets' metadata and their schema-level content combined with faceted navigation. The tool enables filtering on search result sets and also provides various interactive visualizations for assessment. It is also possible to explore individual datasets' metadata and content summaries.

It comprises of two main modules:
* [LODAtlas Datamanager](https://gitlab.inria.fr/epietrig/lodatlas-datamanager)
* [LODAtlas Server](https://gitlab.inria.fr/epietrig/lodatlas-server)

## LODAtlas Datamanager 
LODAtlas datamanager is a tool to download, manipulate and persist the metadata for linked data datasets. It also downloads the RDF dump files for dataset resources specified by the repository, processes it with LODStats to extract used classes, properties and vocabularies. Finally it processes dump files together with ontology files using RDF Summary API to get RDF graph summary. 
## LODAtlas Server
LODAtlas server serves the content processed and indexed by LODAtlas datamanager module through a web interface and REST API. 

The web interface lets users to make keyword and URI search on linked data datasets' metadata and contents combined with faceted navigation. It also provides interactive visualisations.

# Clone repository
The submodules should be cloned as well using `--recurse-submodules`.
```
git clone --recurse-submodules git@gitlab.inria.fr:epietrig/LODAtlas.git
```

# Build
```
mvn clean install -Pprod -Dmaven.test.skip=true
```

# Website


# Documentation
For further information, please refer to [documentation/lodatlasdoc.pdf](https://gitlab.inria.fr/epietrig/LODAtlas/blob/master/documentation/lodatlasdoc.pdf). 

> **Note:**
> This file should be updated with recent changes.

# License
LODAtlas is licensed under [GNU General Public License version 3](https://www.gnu.org/licenses/gpl-3.0.en.html)

