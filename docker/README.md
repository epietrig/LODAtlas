**docker-compose.yml** file allows you to run *LODAtlas-datamanager* and *LODAatlas-server* applications 
without installing the necessary dependencies on your computer. You only need to install docker 
and docker-compose.


# Installation instructions:

## Install docker
Please follow the instructions on docker installation page:
[https://docs.docker.com/install/](https://docs.docker.com/install/)

## Install docker compose
Please follow the instructions on docker compose installation page:
[https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

# Run LODAtlas-datamanager and LODAtlas-server
Simply run the script file that starts docker containers:
```
./run.sh
```

# Configuration details
**LODAtlas-datamanager** requires *MongoDB*, *Postgresql* and *Elasticsearch* to run. 

**LODAtlas-server** requires *Elasticsearch* to run. For each of them, docker-compose.yml file downloads necessary images and configures them.

The lines starting with # in the docker-compose.yml file are either comment lines or disabled 
configuration parameters. To enable these, it is necessary to remove # at the beginning of the line.

  * **Postgres configuration:**
  
   To process large files, the following postgresql config parameters should be set at 
   services.postgres.command line depending on the host machine resources.
```
- shared_buffers
- effective_cache_size
- work_mem
- maintenance_work_mem
- checkpoint_completion_target
- wal_buffers
- default_statistics_target
- max_wal_size
- checkpoint_timeout
```

   To use a local directory as data directory for Postgresql, # sign at the beginning of 
   `services.postgres.volumes` line and the following line should be removed and the **local_directory_path**
   should be replaced with the *local directory path to save Postgresql data*.

  * **Elasticsearch configuration:**
  
   To use a local directory as data directory for Elasticsearch, # sign at the beginning of 
   `services.elasticsearch.volumes` line and the following line should be removed and the 
   **local_directory_path** should be replaced with the *local directory path to save Elasticsearch data*.

  * **MongoDB configuration:**
   
   To use a local directory as data directory for MongoDB, # sign at the beginning of 
   `services.mongo.volumes` line and the following line should be removed and the **local_directory_path**
   should be replaced with the *local directory path to save MongoDB data*.

  * **LODAtlas-datamanager configuration:**
   
   To use a local directory to save logs of LODAtlas-datamanager application, # sign at the beginning
   of `services.datamanager.volumes` line and the line that maps the local directory to 
   `/lodatlas-datamanager/logs` should be removed and the **local_directory_path** should be replaced with 
   the *local logs directory path to save logs*.

To use a local directory to temporarily save and process dump files downloaded by LODAtlas-datamanager
application, # sign at the beginning of `services.datamanager.volumes` line and the line that maps the 
local directory to `/lodatlas-datamanager/tmp` should be removed and the **local_directory_path** should be 
replaced with the *local tmp directory path to save dumps*.

To use a local directory to share files with LODAtlas-datamanager application, # sign at the beginning
of `services.datamanager.volumes` line and the line that maps the local directory to 
`/lodatlas-datamanager/share` should be removed and the **local_directory_path** should be replaced with the 
*local directory path where the necessary file is located*. This is basically necessary to share a different 
repoList.csv file than the default file that the application uses. If a new file is specified, 
`services.datamanager.command` should be changed to have `"-d/lodatlas-datamanager/share/fileName.csv"` 
instead of `"-d/lodatlas-datamanager/repoList.csv"`

  * **repoList.csv format:**
   
Each line in this file specifies a repository which provides a RESTful API to fetch metadata for datasets. The values are as follows:

```
   APIversion, Repo name, Repo URL, query to select relevant datasets, start index, number of datasets
```

   The default repoList.csv contains the following lines:
   
```
ckanv3,datahub,https://old.datahub.io/,q=tags:lod,0,-1
ckanv3,datagov,https://catalog.data.gov/,q=rdf,0,-1
ckanv3,europeandata,https://www.europeandataportal.eu/data/,q=rdf,0,-1
```

>**Notes:**
>  * `docker-compose.yml` is a [YAML](https://en.wikipedia.org/wiki/YAML) file. Make sure not to use tabs, but spaces and pay attention to indentation.
>  * Make sure that ports 5432, 8080, 9200, 9300, 27017 and 9010 are not used by other programs on the 
> host machine.

The LODAtlas-server will serve at [http://localhost:8080/lodatlas](http://localhost:8080/lodatlas).


