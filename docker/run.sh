#!/bin/bash

# necessary for elasticsearch to work
sudo sysctl -w vm.max_map_count=262144

# disable swapping
#sudo swapoff -a

# create images and run
docker-compose up --build